const fs = require("fs");

//var content = fs.readFileSync("./inputVelo.json");

var jsonContent = JSON.parse(fs.readFileSync("city.list.json"));
//console.log('json content : '+ jsonContent);

//var data = jsonContent.data;
//console.log('Data du fichier :'+data);

var data_mapped = jsonContent.map(function (elem){if(elem.country === "FR"){return {id:elem.id, name:elem.name, country:elem.country}}});
data_mapped = data_mapped.filter(function (valeurNulle){
    return valeurNulle != null;
});

var json = JSON.stringify(data_mapped);
fs.writeFile('outputCityFR.json', json, 'utf8');

